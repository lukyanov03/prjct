#include "Node.h"

inline Node::Node()
{
	for (int i = 0; i < 3; i++)
	{
		keys[i] = "";
		List a;
		trans[i] = a;
	}

	for (int i = 0; i < 4; i++)
	{
		child[i] = nullptr;
	}
	parent = nullptr;
}

void Node::insertToNode(const std::string& enWord, const std::string& ruWord)
{
	for (int i = 0; i < 3; i++)
	{
		if (keys[i] == enWord)
		{
			trans[i].append(ruWord);
			return;
		}
		if (keys[i] == "")
		{
			keys[i] = enWord;
			trans[i].append(ruWord);
			this->sort();
			return;
		}
	}
}

void Node::insertToNode(const std::string& key, List& lst)
{
	for (int i = 0; i < 3; i++)
	{
		if (keys[i] == key)
		{
			for (int j = 0; j < lst.size(); j++)
				trans[i].append(lst[j]);

			return;
		}
		if (keys[i] == "")
		{
			keys[i] = key;
			for (int j = 0; j < lst.size(); j++)
				trans[i].append(lst[j]);
			return;
		}
	}
	this->sort();
}


void Node::sort()
{
	size_t size = 0;
	for (int i = 0; i < 3; i++)
	{
		if (keys[i] != "") size++;
	}

	switch (size)
	{
	case 2:
	{
		if (keys[0] > keys[1])
		{
			std::swap(keys[0], keys[1]);
			List tmp = trans[0];
			trans[0] = trans[1];
			trans[1] = tmp;
		}
		break;
	}
	case 3:
	{
		if (keys[0] > keys[1])
		{
			std::swap(keys[0], keys[1]);
			List tmp = trans[0];
			trans[0] = trans[1];
			trans[1] = tmp;
		}
		if (keys[0] > keys[2])
		{
			std::swap(keys[0], keys[2]);
			List tmp = trans[0];
			trans[0] = trans[2];
			trans[2] = tmp;
		}
		if (keys[1] > keys[2])
		{
			std::swap(keys[1], keys[2]);
			List tmp = trans[1];
			trans[1] = trans[2];
			trans[2] = tmp;
		}
		break;
	}
	default:
		break;
	}
}

size_t Node::size()
{
	size_t size = 0;
	for (int i = 0; i < 3; i++)
	{
		if (keys[i] == "") return size;
		size++;
	}
	return size;
}

bool Node::isLeaf()
{
	for (int i = 0; i < 3; i++)
	{
		if (child[i] != nullptr) return false;
	}
	return true;
}

Node::Node(std::string enWord, std::string ruWord)
{
	for (int i = 0; i < 3; i++)
	{
		keys[i] = "";
		List a;
		trans[i] = a;
	}

	for (int i = 0; i < 4; i++)
	{
		child[i] = nullptr;
	}
	parent = nullptr;
	keys[0] = enWord;
	trans[0].append(ruWord);
}

Node::Node(std::string key, List& trans, Node* child1, Node* child2, Node* child3, Node* child4, Node* parent)
{
	Node();
	keys[0] = key;

	this->trans[0] = trans;
	child[0] = child1;
	child[1] = child2;
	child[2] = child3;
	child[3] = child4;
	this->parent = parent;
}

