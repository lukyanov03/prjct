#include "TwoThreeTree.h"

TwoThreeTree::TwoThreeTree() : root(nullptr) {}
TwoThreeTree::~TwoThreeTree()
{
	delete root;
}

Node* TwoThreeTree::insert(std::string enWord, std::string ruWord)
{
	root = insert(root, enWord, ruWord);
	return root;
}


Node* TwoThreeTree::insert(Node* node, std::string key, std::string value)
{
	if (node == nullptr)
	{
		node = new Node(key, value);
		return node;
	}
	if (node->isLeaf()) node->insertToNode(key, value);
	else if (key <= node->keys[0]) insert(node->child[0], key, value);
	else if ((node->size() == 1 or node->size() == 2) and (key <= node->keys[1])) insert(node->child[1], key, value);
	else insert(node->child[2], key, value);

	return split(node);


}

Node* TwoThreeTree::split(Node* node)
{
	if (node->keys[2] == "") return node;


	Node* left = new Node(node->keys[0], node->trans[0], node->child[0], node->child[1], nullptr, nullptr, node->parent);
	Node* right = new Node(node->keys[2], node->trans[2], node->child[2], node->child[3], nullptr, nullptr, node->parent);

	if (left->child[0]) left->child[0]->parent = left;
	if (left->child[1]) left->child[1]->parent = left;
	if (right->child[0]) right->child[0]->parent = right;
	if (right->child[1]) right->child[1]->parent = right;

	if (node->parent)
	{
		node->parent->insertToNode(node->keys[1], node->trans[1]); 

		if (node->parent->child[0] == node) node->parent->child[0] == nullptr;
		else if (node->parent->child[1] == node) node->parent->child[1] == nullptr;
		else if (node->parent->child[2] == node) node->parent->child[2] == nullptr;


		if (node->parent->child[0] == nullptr)
		{
			node->parent->child[3] = node->parent->child[2];
			node->parent->child[2] = node->parent->child[1];
			node->parent->child[1] = right;
			node->parent->child[0] = left;
		}
		else if (node->parent->child[1] == nullptr)
		{
			node->parent->child[3] = node->parent->child[2];
			node->parent->child[2] = right;
			node->parent->child[1] = left;
		}
		else
		{
			node->parent->child[3] = right;
			node->parent->child[2] = left;
		}
		Node* tmp = node->parent;
		delete node;
		return tmp;
	}
	else
	{
		left->parent = node;
		right->parent = node;

		node->child[0] = left;
		node->child[1] = right;
		node->child[2] = nullptr;
		node->child[3] = nullptr;

		node->trans[0].clear();
		node->insertToNode(node->keys[1], node->trans[1]);
		node->trans[0] = node->trans[1];

		node->keys[0] = node->keys[1];
		node->keys[1] = "";
		node->keys[2] = "";
		node->trans[1].clear();
		node->trans[2].clear();

		return node;
	}
}
