#include "List.h"

List::~List()
{
	while (head_ != nullptr)
	{
		NodeList* temp = head_;
		this->head_ = this->head_->next_;
		temp = nullptr;
	}
}

bool List::is_empty()
{
	return head_ == nullptr;
}

NodeList* List::find(const std::string& value)
{
	NodeList* p = head_;
	while (p && p->item_ != value) p = p->next_;
	return (p && p->item_ == value) ? p : nullptr;
}

NodeList* List::append(const std::string& value)
{
	if (head_ == nullptr)
	{
		head_ = new NodeList(value);
		this->size_++;
		return head_;
	}

	if (this->find(value) != nullptr) return nullptr;


	if (this->head_->item_ > value)
	{
		std::string item = head_->item_;
		NodeList* tmp = new NodeList(item, head_->next_, head_);

		if (this->head_->next_ == nullptr)
		{
			head_->item_ = value;
			head_->next_ = tmp;
			size_++;
			return tmp;
		}
		head_->item_ = value;
		head_->next_->prev_ = tmp;
		head_->next_ = tmp;

		this->size_++;
		return tmp;
	}

	else
	{
		NodeList* cur = this->head_;
		this->size_++;

		while (cur->item_ <= value and cur->next_ != nullptr)
		{
			cur = cur->next_;
		}
		if (cur == head_)
		{
			cur = cur->next_;
			cur = new NodeList(value, nullptr, head_);
			head_->next_ = cur;
			return cur;
		}
		if (cur->next_ == nullptr)
		{
			NodeList* node = new NodeList(value, cur, cur->prev_);
			cur->prev_->next_ = node;
			cur->prev_ = node;
			return node;
		}
		else
		{
			NodeList* node = new NodeList(value, cur, cur->prev_);
			cur->prev_->next_ = node;
			cur->prev_ = node;
			return node;
		}
	}
}

bool List::remove(const std::string& str)
{
	auto pos = this->find(str);
	if (!pos) return false;

	if (!pos->prev_)
	{
		size_--;
		head_ = pos->next_;
		delete pos;
		return true;
	}
	else
	{
		if (pos->next_)
		{
			size_--;
			auto prev = pos->prev_;
			pos->prev_->next_ = pos->next_;
			pos->next_->prev_ = prev;
			delete pos;
			return true;

		}
		else
		{
			size_--;
			pos->prev_->next_ = nullptr;
			delete pos;
			return true;
		}
	}
}

std::string& List::operator[](const size_t& i)
{
	NodeList* cur = this->head_;
	size_t j = i;
	while (j > 0)
	{
		cur = cur->next_;
		j--;
	}
	return cur->item_;
}

std::ostream& List::operator<<(std::ostream& out)
{
	NodeList* cur = head_;
	while (cur != nullptr)
	{
		out << cur->item_ << ' ';
		cur = cur->next_;
	}
	out << '\n';
	return out;
}

//List& List::operator=(List& other)
//{
//	if (this != &other)
//	{
//		delete head_;
//		if (other.head_)
//		{
//			NodeList* cur = other.head_;
//			NodeList* newHead = new NodeList(cur->item_);
//			NodeList* newCurrent = newHead;
//			cur = cur->next_;
//
//			while (cur)
//			{
//				newCurrent->next_ = new NodeList(cur->item_);
//				cur = cur->next_;
//				newCurrent = newCurrent->next_;
//			}
//
//			head_ = newHead;
//			size_ = other.size_;
//		}
//		else
//		{
//			head_ = nullptr;
//			size_ = 0;
//		}
//	}
//	return *this;
//}

List List::operator=(List other)
{
	if (this != &other)
	{
		//delete head_;
		if (other.head_)
		{
			NodeList* cur = other.head_;
			NodeList* newHead = new NodeList(cur->item_);
			NodeList* newCurrent = newHead;
			cur = cur->next_;

			while (cur)
			{
				newCurrent->next_ = new NodeList(cur->item_);
				cur = cur->next_;
				newCurrent = newCurrent->next_;
			}

			head_ = newHead;
			size_ = other.size_;
		}
		else
		{
			delete head_;
			size_ = 0;
		}
	}
	return *this;
}

void List::clear()
{
	NodeList* cur = head_;
	while (cur)
	{
		NodeList* nextNode = cur->next_;
		delete cur;
		cur = nextNode;
	}
	size_ = 0;
	head_ = nullptr;
}

std::string& List::front()
{
	return head_->item_;
}

size_t List::size()
{
	return this->size_;
}

NodeList::~NodeList()
{
	next_ = nullptr;
	prev_ = nullptr;
}
