#pragma once
#include "Node.h"

class TwoThreeTree
{
public:
	TwoThreeTree();
	~TwoThreeTree();
	Node* insert(std::string enWord, std::string ruWord);


private:
	Node* insert(Node* node, std::string enWord, std::string ruWord);
	Node* split(Node* node);
	Node* root;
};

