#pragma once

#include <string>
#include <iostream>


struct NodeList
{
	std::string item_;
	NodeList* next_;
	NodeList* prev_;
	NodeList(const std::string& str, NodeList* next = nullptr, NodeList* prev = nullptr) : item_(str), next_(next), prev_(prev) {}
	NodeList() : item_(""), next_(nullptr), prev_(nullptr) {}
	~NodeList();
};

class List
{

private:

	NodeList* head_;
	size_t size_;

public:
	List() : head_(nullptr), size_(0) {}

	~List();

	bool is_empty();
	NodeList* find(const std::string& value);

	NodeList* append(const std::string& value);

	bool remove(const std::string& str);

	std::string& operator[](const size_t& i);

	std::ostream& operator<<(std::ostream& out);

	//List& operator=(List& other);
	List operator=(List other);


	void clear();

	std::string& front();

	size_t size();
};


