#pragma once
#include "List.h"

struct Node
{
	std::string keys[3];

	List trans[3];

	Node* child[4];
	Node* parent;

	Node();
	~Node() = default;

	void insertToNode(const std::string& key, const std::string& value);
	void insertToNode(const std::string& key, List& lst);

	void sort();
	size_t size();
	bool isLeaf();


	Node(std::string key, std::string value);
	Node(std::string key, List& trans, Node* child1, Node* child2, Node* child3, Node* child4, Node* parent);
};














